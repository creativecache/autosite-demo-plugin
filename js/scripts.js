jQuery(document).ready(function($) {

	function toggleContainerTypes(containerType, containerSize) {

		if (containerType == 'container') {
			$('.container-full').each(function() {
				$(this).toggleClass('container container-full')
			});
		} else if (containerType == 'container-full') {
			$('.container').each(function() {
				$(this).toggleClass('container-full container')
			});
		}

		if (containerSize == 'sm') {
			$('.wrapper').each(function() {
				$(this).removeClass('md-container lg-container xl-container').addClass('sm-container');
			});
		} else if (containerSize == 'md') {
			$('.wrapper').each(function() {
				$(this).removeClass('sm-container lg-container xl-container').addClass('md-container');
			});
		} else if (containerSize == 'lg') {
			$('.wrapper').each(function() {
				$(this).removeClass('sm-container md-container xl-container').addClass('lg-container');
			});
		} else if (containerSize == 'xl') {
			$('.wrapper').each(function() {
				$(this).removeClass('sm-container md-container lg-container').addClass('xl-container');
			});
		} else {
			$('.wrapper').each(function() {
				$(this).removeClass('sm-container md-container lg-container xl-container');
			});
		}

	}

	function headerOptions(headerBackground, fixed) {
		if (headerBackground == 'primary') {
			$('header').removeClass('secondary-bg tertiary-bg white-bg black-bg transparent-bg').addClass('primary-bg');
		} else if (headerBackground == 'secondary') {
			$('header').removeClass('primary-bg tertiary-bg white-bg black-bg transparent-bg').addClass('secondary-bg');
		} else if (headerBackground == 'tertiary') {
			$('header').removeClass('primary-bg secondary-bg white-bg black-bg transparent-bg').addClass('tertiary-bg');
		} else if (headerBackground == 'white') {
			$('header').removeClass('primary-bg tertiary-bg secondary-bg black-bg transparent-bg').addClass('white-bg');
		} else if (headerBackground == 'black') {
			$('header').removeClass('primary-bg tertiary-bg white-bg secondary-bg transparent-bg').addClass('black-bg');
		} else if (headerBackground == 'transparent') {
			$('header').removeClass('primary-bg tertiary-bg white-bg black-bg secondary-bg').addClass('transparent-bg');
		}

		if (fixed == 'fixed') {
			$('header').addClass('fixed');
		} else {
			$('header').removeClass('fixed');
		}
	}

	function addClasses(name) {

		if ( name == 'default' ) {
			toggleContainerTypes('container', 'lg');
			headerOptions('primary');
		}

		if ( name == 'architect' ) {
			toggleContainerTypes('container-full');
			headerOptions('white');

		}

		if ( name == 'blogger' ) {
			toggleContainerTypes('container', 'sm');
			headerOptions('secondary');
		}

	}

	function wipeClasses(name) {

		addClasses(name);
	}

	function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		createCookie(name,"",-1);
	}

	function plugin_init() {
		var demoBarHeight = $('#autosite-demo-bar').outerHeight(),
			demoName = readCookie('autosite-demo-name'),
			demoType = readCookie('autosite-demo-type');

		$('body').css('padding-bottom', demoBarHeight);

		if ( (demoName) && (demoType) ) {
			var nameOnly = demoName.replace('demo-', '');
			$('body').addClass(demoName + ' ' + demoType);
			$('#autosite-theme-select option[value="' + nameOnly + '"]').attr("selected",true);
			wipeClasses(nameOnly);
		} else {
			$('body').addClass('demo-default demo-standard');
		}
		
	}

	function changeDemo(name, type) {

		if (name == 'default') {
			eraseCookie('autosite-demo-name');
			eraseCookie('autosite-demo-type');
		} else {
			createCookie('autosite-demo-name', 'demo-' + name, 0.4166);
			createCookie('autosite-demo-type', 'demo-' + type, 0.4166);
		}

		$('body').removeClass (function (index, className) {
		    return (className.match (/(^|\s)demo-\S+/g) || []).join(' ');
		});

		$('body').addClass('demo-' + name + ' ' + 'demo-' + type);

		wipeClasses(name);

		if (type == 'blog') {
			//window.location.href = "/blog";
		}
	}

	plugin_init();

	$('#autosite-theme-select').change(function() {
		var templateName = $(this).children('option:selected').attr('value'),
			templateType = $(this).children('option:selected').attr('data-sitetype');

		changeDemo(templateName, templateType);
	});



});