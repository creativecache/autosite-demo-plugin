<?php
/*
Plugin Name: Autosite Demo
Plugin URI: https://autosite.io/
Description: Used to show the demo functionality of Autosite theme
Version: 1.0.0
Author: Autosite
Author URI: https://autosite.io/
License: GPLv2 or later
Text Domain: autosite-demo
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}


// Enqueue files
if ( ! function_exists( 'autosite_demo_scripts' ) ) {

	// Load plugin's JavaScript and CSS sources
	function autosite_demo_scripts() {
		// Get the plugin data
		$the_plugin = get_file_data( __FILE__, array('Version' => 'Version'), 'plugin');
		$plugin_version = $the_plugin['Version'];

		wp_enqueue_style( 'autosite-demo-styles', plugin_dir_url(__FILE__) . 'css/styles.css', array('autosite-styles'), $plugin_version );


		wp_enqueue_script( 'jquery' );

		wp_enqueue_script( 'autosite-demo-scripts', plugin_dir_url(__FILE__) . 'js/scripts.js', array('autosite-scripts'), $plugin_version, true );
	}
}
add_action( 'wp_enqueue_scripts', 'autosite_demo_scripts' );

// Add bar to page
function autosite_demo_bar() {
    include plugin_dir_path( __FILE__ ) . 'inc/footer-bar-content.php';
}
add_action('wp_footer', 'autosite_demo_bar');