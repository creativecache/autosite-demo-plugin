<noscript>
	<style type="text/css">
		#autosite-demo-bar { display: none; }
	</style>
</noscript>

<div id="autosite-demo-bar" class="flex-container flex-middle flex-between">
	<p>See our theme in action!</p>
	<div class="demo-template-select">
		<label for="autosite-theme-select">Current preset:</label>
		<select id="autosite-theme-select">
			<option value="default" data-sitetype="standard">Default</option>
			<option value="architect" data-sitetype="standard">The Architect</option>
			<option value="blogger" data-sitetype="blog">The Blogger</option>
		</select>
	</div>
</div>